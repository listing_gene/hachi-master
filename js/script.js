$(function () {
  // #で始まるリンクをクリックしたら実行されます
  $('a[href^="#"]').click(function () {
      // スクロールの速度
      var speed = 600; // ミリ秒で記述
      var href = $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top;
      $('body,html').animate({ scrollTop: position }, speed, 'swing');
      return false;
  });
});

$(window).on('load scroll', function() {
  fix_element();
});
 
function fix_element() {
  // 固定配置に使用する要素
  var $fixWrapper = $('header');
  var $fix = $('.float');
 
  // 要素がある場合のみ処理
  if($fixWrapper.length && $fix.length) {
    var fixTop = $fixWrapper.offset().top;
    var winScroll = $(window).scrollTop();
    // 対象位置を通過している場合
    if(winScroll > fixTop) {
      $fix.addClass('fixed');
    } else {
      $fix.removeClass('fixed');
    }
  }
}

// お客様の声
$windowWidth = window.innerWidth;
$windowTb = 750;
$windowPc = 1200;
if ($windowWidth < $windowTb) {
  $(function () {
      $('.voice_list').slick({
          dots: true, /* 画像下の点を表示 */
          speed: 500,
          infinite: true, /* スライドのループ有効化 */
          autoplaySpeed: 3000,
      });
  });
} else if ($windowWidth >= $windowTb && $windowWidth < $windowPc) {
  $(function () {
      $('.voice_list').slick({
          dots: true, /* 画像下の点を表示 */
          accessibility: true,
          speed: 500,
          infinite: true, /* スライドのループ有効化 */
          autoplaySpeed: 5000,
          centerMode: true,
          centerPadding: '20%',
          centeredSlides: true,
      });
  });
} else if ($windowWidth >= $windowPc) {
  $(function () {
      $('.voice_list').slick({
          dots: true, /* 画像下の点を表示 */
          accessibility: true,
          speed: 500,
          infinite: true, /* スライドのループ有効化 */
          autoplaySpeed: 5000,
          centerMode: true,
          centerPadding: '27%',
          centeredSlides: true,
      });
  });
}